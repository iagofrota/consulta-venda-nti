<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app"
     class="container">
    <div class="text-sm-center">
        <h1>Consulta Venda NTI</h1>
    </div>

    <div style="display: flex; justify-content: center; margin-top: 25px">
        <form action="">
            <div class="row">
                <div class="col-sm-12 mr-auto">
                    <div class="form-group row">
                        <label for="cpf" class="col-sm-2 col-form-label">CPF</label>
                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control cpf"
                                   id="cpf"
                                   name="cpf"
                                   value="{{ !empty($_GET['cpf']) ? $_GET['cpf'] : null }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" id="btn-search" class="btn btn-primary">Pesquisar</button>
                </div>
            </div>
        </form>
    </div>

    <br>
    <br>
    <br>

    <div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Minhas Compras</h1>
            </div>

            <div class="col-sm-12">
                <p><b>Cliente:</b> <span id="client"></span></p>
            </div>

            <div class="col-sm-12">
                <table class="table table-striped table-sale">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Data</th>
                        <th>Desconto</th>
                        <th>Total sem Desconto</th>
                        <th>Total com Desconto</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody class="tbody-sale">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal-dialog modal-lg">
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Produtos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped table-product">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Quantidade</th>
                                    <th>Preço</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-product">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
