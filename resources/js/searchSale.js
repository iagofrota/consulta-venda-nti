$(function () {
    const btnSearch = $('#btn-search')
    const cpf = $('#cpf')

    if (cpf.val() != '') {
        $.ajax({
            url: "http://vendasnti.test/api/client",
            type: "POST",
            async: true,
            data: {
                cpf: cpf.val(),
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)

                reloadTableProduct(data)
            },
            error: function (data) {
                console.log(data)
            }
        });
    }

    btnSearch.click(function () {
        $.ajax({
            url: "http://vendasnti.test/api/client",
            type: "POST",
            async: true,
            data: {
                cpf: cpf.val(),
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)

                reloadTableProduct(data)
            },
            error: function (data) {
                console.log(data)
            }
        });
    })
})

function reloadTableProduct(sales) {
    const tableSale = $('.table-sale')
    const tbodySale = $('.tbody-sale')
    const client = $('#client')

    tbodySale.empty()

    client.text(sales[0].client.name)

    $.each(sales, function (index, value) {
        let novaLinhaEl = $("<tr>");
        let linhas = `
                    <td>${value.public_id}</td>
                    <td>${value.created_at}</td>
                    <td>${value.discount}</td>
                    <td>${value.total_without_discount.toLocaleString("pt-BR", {
                            minimumFractionDigits: 2,
                            style: 'currency',
                            currency: 'BRL'
                        })}
                    </td>
                    <td>${value.total_with_discount.toLocaleString("pt-BR", {
                            minimumFractionDigits: 2,
                            style: 'currency',
                            currency: 'BRL'
                        })}
                    </td>
                    <td>
                        <button type="button"
                                class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#exampleModal">
                            Visualizar
                        </button>
                    </td>
                    `

        novaLinhaEl.append(linhas)
        tableSale.append(novaLinhaEl)
    })
}

function modalProduct(sales) {
    const tableSale = $('.table-product')
    const tbodySale = $('.tbody-product')

    tbodySale.empty()

    $.each(sales, function (index, value) {
        let novaLinhaEl = $("<tr>");
        let linhas = `
                    <td>${value.public_id}</td>
                    <td>${value.created_at}</td>
                    <td>${value.discount}</td>
                    <td>${value.total_without_discount.toLocaleString("pt-BR", {
            minimumFractionDigits: 2,
            style: 'currency',
            currency: 'BRL'
        })}
                    </td>
                    <td>${value.total_with_discount.toLocaleString("pt-BR", {
            minimumFractionDigits: 2,
            style: 'currency',
            currency: 'BRL'
        })}
                    </td>
                    `

        novaLinhaEl.append(linhas)
        tableSale.append(novaLinhaEl)
    })
}
