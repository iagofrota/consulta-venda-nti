# Consulta Venda NTI
Sistema de consulta de vendas pelo cliente para o processo seletivo para a vaga de Analista PHP para o NTI da UVA

## Autor 
[Iago Olímppio Frota](https://iagofrota.com.br/)

## Tecnologias utilizadas
1. PHP 7.2
1. Laravel Framework 6.18.40
1. Postgres 12
1. JQuery 3.2
1. Bootstrap 4.5.2

## Configurações para rodar o projeto
1. Clonar o projeto `git clone https://gitlab.com/iagofrota/consulta-venda-nti.git`
1. Entre na pasta do projeto clonado e execute os seguintes comandos
    1. `composer install`
    1. `php artisan key:generate`
    1. `npm install`
    1. `npm run dev`

## Intruções de uso
Para pesquisar os dados das vendas de um cliente, você deverá informar um CPF.
